package com.yakovliam.spaceanticheat;

import com.yakovliam.spaceanticheat.command.SpaceAntiCheatCommand;
import com.yakovliam.spaceanticheat.configuration.Config;
import com.yakovliam.spaceanticheat.listener.JoinQuitListener;
import com.yakovliam.spaceanticheat.listener.PacketListener;
import com.yakovliam.spaceanticheat.listener.behavior.BehaviorListener;
import com.yakovliam.spaceanticheat.stats.MetricsHandler;
import com.yakovliam.spaceapi.abstraction.BukkitPlugin;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpaceAntiCheat extends JavaPlugin {

    @Getter
    private static SpaceAntiCheat instance;

    @Getter
    @Setter
    private BukkitPlugin api;

    @Getter
    private Config sacConfig;

    @Override
    public void onLoad() {
        // init instance
        instance = this;
    }

    @Override
    public void onEnable() {
        initApi();

        // init config
        sacConfig = new Config();

        // init listeners
        this.getServer().getPluginManager().registerEvents(new JoinQuitListener(), this);
        this.getServer().getPluginManager().registerEvents(new BehaviorListener(), this);
        new PacketListener();

        // register command
        new SpaceAntiCheatCommand();

        // init metrics
        new MetricsHandler();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void initApi() {
        // init api
        this.api = new BukkitPlugin(instance);
    }
}
