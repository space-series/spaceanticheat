package com.yakovliam.spaceanticheat.alert;

import com.yakovliam.spaceanticheat.check.Check;
import com.yakovliam.spaceanticheat.check.CheckVersion;
import com.yakovliam.spaceanticheat.configuration.Config;
import com.yakovliam.spaceanticheat.model.user.User;
import com.yakovliam.spaceapi.command.BukkitSpaceCommandSender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static com.yakovliam.spaceanticheat.configuration.Config.ALERT_PER_PLAYER_DELAY_MILLIS;
import static com.yakovliam.spaceanticheat.configuration.Config.PERMISSIONS_ALERTS;
import static com.yakovliam.spaceanticheat.util.Messages.ALERT;

@AllArgsConstructor
public class Alert {

    /**
     * The version of the check that was alerted to
     */
    @Getter
    private CheckVersion checkVersion;

    /**
     * The check that was alerted to
     */
    @Getter
    private Check check;

    /**
     * The target user
     */
    @Getter
    private User user;

    public void alert() {
        // generate player's username
        Player target = Bukkit.getPlayer(user.getUuid());

        // check status (if online)
        if (target == null || !target.isOnline()) return;

        // get last alert time...if it is within [THRESHOLD], then do NOT alert
        long lastAlertTime = user.getLastAlertTime();
        long currentTime = System.currentTimeMillis();
        long threshold = ALERT_PER_PLAYER_DELAY_MILLIS.get(Config.get());

        // if within threshold to NOT alert
        if((currentTime - lastAlertTime) < threshold) return;

        // loop through all players online who have the correct permission node
        String applicableNode = PERMISSIONS_ALERTS.get(Config.get());

        Bukkit.getOnlinePlayers()
                .stream()
                .filter(player -> player.hasPermission(applicableNode))
                .map(BukkitSpaceCommandSender::new)
                .forEach(bukkitSpaceCommandSender -> {
                    // send alert message
                    ALERT.msg(bukkitSpaceCommandSender,
                            "%player%", target.getName(),
                            "%type%", StringUtils.capitalize(check.getType().name().toLowerCase()),
                            "%version%", checkVersion.getHandle(),
                            "%certainty%", Double.toString(checkVersion.calculateCertainty()));
                });

        // set last alert time
        user.setLastAlertTime(currentTime);
    }
}
