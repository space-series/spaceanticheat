package com.yakovliam.spaceanticheat.check;

import com.comphenix.protocol.events.PacketEvent;

public interface Callable {

    /**
     * Called when a packet is sent by a player
     *
     * @param packetEvent The packet event
     */
    void check(PacketEvent packetEvent);

}
