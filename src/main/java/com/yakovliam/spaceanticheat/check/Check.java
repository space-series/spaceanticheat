package com.yakovliam.spaceanticheat.check;

import com.yakovliam.spaceanticheat.model.user.User;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class Check {

    /**
     * The user that the check is applicable to
     */
    @Getter
    private User user;

    /**
     * The type of the check
     */
    @Getter
    private CheckType type;

    /**
     * The different versions that exist
     */
    @Getter
    private Map<String, CheckVersion> checkVersions;

    /**
     * Initializes check
     *
     * @param user The target user
     * @param type The check type
     */
    public Check(User user, CheckType type) {
        this.user = user;
        this.type = type;

        this.checkVersions = new HashMap<>();
    }

    /**
     * Adds a new check version
     *
     * @param checkVersion The check version to add
     */
    public void addVersion(CheckVersion checkVersion) {
        this.checkVersions.put(checkVersion.getHandle(), checkVersion);
    }

    /**
     * Gets the CheckVersion associated with the handle (which is the version!)
     *
     * @param handle The handle or version of the check
     * @return The target CheckVersion
     */
    public CheckVersion getVersion(String handle) {
        return checkVersions.getOrDefault(handle, null);
    }

    public double getCertainty() {
        double totalCertainty = 0;

        // get total calls & flagged calls
        for (CheckVersion checkVersion : checkVersions.values()) {
            totalCertainty += checkVersion.calculateCertainty();
        }

        // calculate
        return (totalCertainty / checkVersions.size() * 100D) * 100D;
    }
}

