package com.yakovliam.spaceanticheat.check;

import com.yakovliam.spaceanticheat.alert.Alert;
import lombok.Getter;
import lombok.Setter;

public abstract class CheckVersion implements Callable {

    /**
     * The current check of the check version
     */
    @Getter
    @Setter
    private Check check;

    /**
     * The version of the check (e.g. "A", "B", "C", etc)
     */
    @Getter
    @Setter
    private String handle;

    /**
     * The total number of calls to the check that was made
     */
    @Getter
    @Setter
    private int totalCalls;

    /**
     * The total number of calls in which the check thought
     */
    @Getter
    @Setter
    private int flaggedCalls;

    /**
     * Initializes check version
     *
     * @param check  The check
     * @param handle The check version
     */
    public CheckVersion(Check check, String handle) {
        this.check = check;
        this.handle = handle;
    }

    /**
     * Calculates the certainty that the target player is cheating
     *
     * @return The calculated certainty
     */
    public double calculateCertainty() {
        //TODO Change calculation method
        return flaggedCalls;
//        return ((double) flaggedCalls / totalCalls) * 100D;
    }

    /**
     * Called when a check is executed
     *
     * @param isCheating If the target player is cheating
     */
    public void callback(boolean isCheating) {
        // increment total calls
        totalCalls++;

        if (isCheating) {
            // increment flagged calls
            flaggedCalls++;

            // alert
            new Alert(this, check, this.check.getUser()).alert();
        }

    }

}
