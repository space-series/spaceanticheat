package com.yakovliam.spaceanticheat.check.checks.speed;

import com.yakovliam.spaceanticheat.check.Check;
import com.yakovliam.spaceanticheat.check.CheckType;
import com.yakovliam.spaceanticheat.model.user.User;

public class SpeedCheck extends Check {

    /**
     * Initializes check
     *
     * @param user The target user
     */
    public SpeedCheck(User user) {
        super(user, CheckType.SPEED);

        // add versions
        addVersion(new SpeedCheckA(this, "A"));
    }
}
