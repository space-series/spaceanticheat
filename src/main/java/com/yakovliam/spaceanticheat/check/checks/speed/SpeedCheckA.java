package com.yakovliam.spaceanticheat.check.checks.speed;

import com.comphenix.protocol.events.PacketEvent;
import com.yakovliam.spaceanticheat.check.Check;
import com.yakovliam.spaceanticheat.check.CheckVersion;
import com.yakovliam.spaceanticheat.configuration.Config;
import com.yakovliam.spaceanticheat.model.user.behavior.Behavior;
import com.yakovliam.spaceanticheat.script.MathExpression;
import org.bukkit.Location;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import static com.yakovliam.spaceanticheat.configuration.Config.*;

public class SpeedCheckA extends CheckVersion {

    private MathExpression mathExpression;

    /**
     * Initializes check version
     *
     * @param check  The check
     * @param handle The check version
     */
    public SpeedCheckA(Check check, String handle) {
        super(check, handle);
    }

    /**
     * Called when a packet is sent by a player
     *
     * @param packetEvent The packet event
     */
    @Override
    public void check(PacketEvent packetEvent) {

        // get behavior data
        Behavior behavior = getCheck().getUser().getBehavior();

        // if player is creative or spectator, return
        if (behavior.isCreativeOrSpectator()) return;

        // get potion effects, check if speed is enabled
        boolean hasSpeed = behavior.getPotionEffects().stream().anyMatch(potionEffect -> potionEffect.getType() == PotionEffectType.SPEED);

        // if has speed, get level and check with configuration
        if (hasSpeed) {
            PotionEffect effect = behavior.getPotionEffects().stream().filter(potionEffect -> potionEffect.getType() == PotionEffectType.SPEED).findFirst().orElse(null);
            if (effect != null && effect.getAmplifier() >= CHECKS_SPEED_DO_NOT_CHECK_SPEED_POTION_OVER_LEVEL.get(Config.get())) {
                return;
            }
        }

        // get current and last move
        Location current = behavior.getCurrentMove();
        Location last = behavior.getLastMove();

        if (current == null || last == null) {
            callback(false);
            return;
        }

        // calculate distance
        double x1 = last.getX();
        double y1 = last.getY();
        double z1 = last.getZ();

        double x2 = current.getX();
        double y2 = current.getY();
        double z2 = current.getZ();

        double dist = Math.sqrt(0
                + Math.pow((x2 - x1), 2)
                + Math.pow((y2 - y1), 2)
                + Math.pow((z2 - z1), 2)
        );

        // init math expression
        if (mathExpression == null) mathExpression = new MathExpression();

        // get returned response from expression
        Object pingMultiplierResult = mathExpression
                .evaluate(CHECKS_SPEED_PING_MULTIPLIER_EXPRESSION.get(Config.get())
                        .replace("%ping%", Integer.toString(behavior.getPing())));

        if (pingMultiplierResult != null) {
            try {
                // try to get integer from result
                Integer result = (Integer) pingMultiplierResult;

                // add to distance
                dist += result;
            } catch (Exception ignored) {
            }
        }

        /* DESCENDING BY */
        // get descending by num
        double descendingBy = Math.abs(y2 - y1);
        // if descending by more than the threshold, cancel
        if (descendingBy >= CHECKS_SPEED_DO_NOT_CHECK_ASC_DESC_BY.get(Config.get())) {
            return;
        }

        // check walk speed
        double walkSpeed = behavior.getWalkSpeed();
        if (walkSpeed >= CHECKS_SPEED_NO_NOT_CHECK_WALK_SPEED.get(Config.get())) {
            return;
        }

        // check flying
        boolean flyEnabled = behavior.getFlyEnabled();
        if (flyEnabled && CHECKS_SPEED_NO_NOT_CHECK_FLY_ENABLED.get(Config.get())) return;

        // if dist is over threshold, call flag
        if (dist >= CHECKS_SPEED_ALERT_THRESHOLD_ON_DISTANCE.get(Config.get())) {
            callback(true);
            return;
        }

        callback(false);
    }
}
