package com.yakovliam.spaceanticheat.command;

import com.yakovliam.spaceanticheat.SpaceAntiCheat;
import com.yakovliam.spaceapi.command.Command;
import com.yakovliam.spaceapi.command.Permissible;
import com.yakovliam.spaceapi.command.PlayersOnly;
import com.yakovliam.spaceapi.command.SpaceCommandSender;

import java.util.Arrays;

import static com.yakovliam.spaceanticheat.util.Messages.HELP;

@PlayersOnly
@Permissible("spaceanticheat.command")
public class SpaceAntiCheatCommand extends Command {

    public SpaceAntiCheatCommand() {
        super(SpaceAntiCheat.getInstance().getApi(), "spaceanticheat", "Main SpaceAntiCheat Command", Arrays.asList(
                "ac",
                "anticheat"
        ));
    }

    @Override
    public void onCommand(SpaceCommandSender spaceCommandSender, String s, String... args) {
        HELP.msg(spaceCommandSender);
    }
}
