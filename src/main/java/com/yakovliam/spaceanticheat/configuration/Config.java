package com.yakovliam.spaceanticheat.configuration;

import com.yakovliam.spaceanticheat.SpaceAntiCheat;
import com.yakovliam.spaceapi.config.BukkitConfig;
import com.yakovliam.spaceapi.config.adapter.BukkitConfigurateAdaption;
import com.yakovliam.spaceapi.config.keys.ConfigKey;
import com.yakovliam.spaceapi.config.keys.ConfigKeyTypes;

public class Config extends BukkitConfig {

    public Config() {
        super(SpaceAntiCheat.getInstance(), "config.yml");
    }

    /**
     * Gets the adaption for the target config
     *
     * @return The adaption
     */
    public static BukkitConfigurateAdaption get() {
        return new BukkitConfigurateAdaption(SpaceAntiCheat.getInstance(), SpaceAntiCheat.getInstance().getSacConfig());
    }

    /* Keys below */
    public static ConfigKey<String> PERMISSIONS_ALERTS = ConfigKeyTypes.stringKey("permissions.alert", "spaceanticheat.alert");

    public static ConfigKey<Long> ALERT_PER_PLAYER_DELAY_MILLIS = ConfigKeyTypes.longKey("alert.per-player-delay-millis", 1000);

    /* CHECKS */
    public static ConfigKey<Integer> CHECKS_SPEED_DO_NOT_CHECK_SPEED_POTION_OVER_LEVEL = ConfigKeyTypes.integerKey("checks.speed.doNotCheckIfSpeedPotionEffectOverLevel", null);
    public static ConfigKey<Double> CHECKS_SPEED_DO_NOT_CHECK_ASC_DESC_BY = ConfigKeyTypes.doubleKey("checks.speed.doNotCheckIfAscDescBy", -1);
    public static ConfigKey<String> CHECKS_SPEED_PING_MULTIPLIER_EXPRESSION = ConfigKeyTypes.stringKey("checks.speed.pingMultiplierExpression", null);
    public static ConfigKey<Double> CHECKS_SPEED_ALERT_THRESHOLD_ON_DISTANCE = ConfigKeyTypes.doubleKey("checks.speed.alertThresholdOnDistance", -1);
    public static ConfigKey<Double> CHECKS_SPEED_NO_NOT_CHECK_WALK_SPEED = ConfigKeyTypes.doubleKey("checks.speed.doNotCheckIfWalkSpeed", -1);
    public static ConfigKey<Boolean> CHECKS_SPEED_NO_NOT_CHECK_FLY_ENABLED = ConfigKeyTypes.booleanKey("checks.speed.doNotCheckIfFlyEnabled", true);

}
