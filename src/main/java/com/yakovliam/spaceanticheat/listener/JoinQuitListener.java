package com.yakovliam.spaceanticheat.listener;

import com.yakovliam.spaceanticheat.model.user.UserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        // add to user manager
        UserManager.get(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        // add to user manager
        UserManager.invalidate(event.getPlayer().getUniqueId());
    }
}
