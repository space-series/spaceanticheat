package com.yakovliam.spaceanticheat.listener;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.yakovliam.spaceanticheat.SpaceAntiCheat;
import com.yakovliam.spaceanticheat.check.CheckType;
import com.yakovliam.spaceanticheat.check.CheckVersion;
import com.yakovliam.spaceanticheat.model.user.UserManager;
import org.bukkit.entity.Player;

public class PacketListener {

    /**
     * Active protocol manager
     */
    private final ProtocolManager protocolManager;

    /**
     * Init packet listener
     */
    public PacketListener() {
        protocolManager = ProtocolLibrary.getProtocolManager();

        listen();
    }

    /**
     * Listens for incoming packets
     */
    public void listen() {

        // Listens for movement
        protocolManager.addPacketListener(new PacketAdapter(SpaceAntiCheat.getInstance(), ListenerPriority.LOWEST, PacketType.Play.Client.POSITION) {

            @Override
            public void onPacketReceiving(PacketEvent event) {
                // send new packet for 'test'
                getCheckVersion(event.getPlayer(), CheckType.SPEED, "A").check(event);
            }
        });
    }

    /**
     * Gets a check version by handle and type
     *
     * @param target The target player
     * @param type The check type
     * @param version The check version
     * @return The check
     */
    private CheckVersion getCheckVersion(Player target, CheckType type, String version) {
        return (UserManager.get(target.getUniqueId()))
                .getCheck(type)
                .getVersion(version);
    }

}
