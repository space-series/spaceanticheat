package com.yakovliam.spaceanticheat.listener.behavior;

import com.yakovliam.spaceanticheat.model.user.User;
import com.yakovliam.spaceanticheat.model.user.UserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class BehaviorListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMove(PlayerMoveEvent event) {
        // get user behavior, set last move
        User user = UserManager.get(event.getPlayer().getUniqueId());

        user.getBehavior().setCurrentMove(event.getTo());
        user.getBehavior().setLastMove(event.getFrom());
    }
}
