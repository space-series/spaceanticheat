package com.yakovliam.spaceanticheat.model.status;

public enum  CheatingStatus {
    DEFINITELY,
    MAYBE,
    PROBABLY_NOT,
    UNKNOWN
}
