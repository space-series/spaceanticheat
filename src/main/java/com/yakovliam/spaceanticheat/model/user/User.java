package com.yakovliam.spaceanticheat.model.user;

import com.yakovliam.spaceanticheat.check.Check;
import com.yakovliam.spaceanticheat.check.CheckType;
import com.yakovliam.spaceanticheat.check.checks.speed.SpeedCheck;
import com.yakovliam.spaceanticheat.model.status.CheatingStatus;
import com.yakovliam.spaceanticheat.model.user.behavior.Behavior;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    @Getter
    @Setter
    private UUID uuid;

    @Getter
    @Setter
    private List<Check> checkList;

    @Getter
    @Setter
    private CheatingStatus cheatingStatus;

    @Getter
    @Setter
    private long lastAlertTime;

    @Getter
    @Setter
    private Behavior behavior;

    public User(UUID uuid) {
        // uuid
        this.uuid = uuid;

        // check list
        this.checkList = new ArrayList<>(CheckType.values().length);

        // current status of cheating or not
        this.cheatingStatus = CheatingStatus.UNKNOWN;

        // add checks to the user
        addChecks();

        // behavior
        this.behavior = new Behavior(this);
    }

    private void addChecks() {
        checkList.add(new SpeedCheck(this));
    }

    public Check getCheck(CheckType checkType) {
        return checkList.stream().filter(check -> check.getType().equals(checkType)).findFirst().orElse(null);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.uuid);
    }
}
