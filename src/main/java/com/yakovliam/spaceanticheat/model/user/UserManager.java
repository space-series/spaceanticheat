package com.yakovliam.spaceanticheat.model.user;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserManager {

    /* THIS IS A TEMPORARY SOLUTION - WILL BE REPLACED WITH A DATABASE AT SOME POINT */

    private static Map<UUID, User> userMap = new HashMap<>();

    /**
     * Gets a user
     *
     * @param uuid The uuid of the user
     * @return The user
     */
    public static User get(UUID uuid) {
        User target = userMap.get(uuid);

        if (target == null) {
            target = new User(uuid);
            userMap.put(uuid, target);
        }

        return target;
    }

    /**
     * Removes a user
     *
     * @param uuid The uuid of the user
     */
    public static void invalidate(UUID uuid) {
        userMap.remove(uuid);
    }
}
