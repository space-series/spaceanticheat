package com.yakovliam.spaceanticheat.model.user.behavior;

import com.yakovliam.spaceanticheat.model.user.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Behavior {

    @Getter
    @Setter
    private User user;

    @Getter
    @Setter
    private Location lastMove;

    @Setter
    @Getter
    private Location currentMove;

    public Behavior(User user) {
        this.user = user;
    }

    public boolean isCreativeOrSpectator() {
        Player player = getUser().getPlayer();
        GameMode gameMode = player.getGameMode();
        return gameMode == GameMode.CREATIVE || gameMode == GameMode.SPECTATOR;
    }

    public List<PotionEffect> getPotionEffects() {
        return new ArrayList<>(getUser().getPlayer().getActivePotionEffects());
    }

    public double getWalkSpeed() {
        return getUser().getPlayer().getWalkSpeed();
    }

    public boolean getFlyEnabled() {
        return getUser().getPlayer().getAllowFlight() && getUser().getPlayer().isFlying();
    }

    public int getPing() {
        int ping = -1;

        // get ping using reflection
        try {
            Method getHandleMethod = getUser().getPlayer().getClass().getDeclaredMethod("getHandle");
            Object nmsPlayer = getHandleMethod.invoke(getUser().getPlayer());
            Field pingField = nmsPlayer.getClass().getDeclaredField("ping");
            ping = pingField.getInt(nmsPlayer);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ping;
    }
}
