package com.yakovliam.spaceanticheat.script;

public interface Expression {

    /**
     * Evaluates a certain expression from string
     *
     * @param input The input
     * @return The output
     */
    Object evaluate(String input);

}
