package com.yakovliam.spaceanticheat.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MathExpression implements Expression {

    /**
     * Evaluates a certain expression from string
     *
     * @param input The input
     * @return The output
     */
    @Override
    public Object evaluate(String input) {
        // get script manager
        ScriptEngineManager mgr = new ScriptEngineManager();
        // get scripe engine for JS
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        // evaluate
        Object response = null;
        try {
            response = engine.eval(input);
        } catch (ScriptException e) {
            e.printStackTrace();
        }

        return response;
    }
}
