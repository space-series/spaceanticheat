package com.yakovliam.spaceanticheat.stats;

import com.yakovliam.spaceanticheat.SpaceAntiCheat;
import org.bstats.bukkit.Metrics;

public class MetricsHandler {

    public MetricsHandler() {
        init();
    }

    private static final int pluginId = 7615;

    public static void init() {
        // init metrics
        new Metrics(SpaceAntiCheat.getInstance(), pluginId);
    }
}
