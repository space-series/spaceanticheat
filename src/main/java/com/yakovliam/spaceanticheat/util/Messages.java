package com.yakovliam.spaceanticheat.util;

import com.yakovliam.spaceapi.text.Message;

public class Messages {

    public static Message HELP = Message.builder("help", true)
            .info("&7&m        &r &c&lSpaceAntiCheat &7&m        ")
            .build();

    public static Message ALERT = Message.builder("alert", true)
            .info("&3&lSpaceAntiCheat &8| &f%player% &7failed &f%type% &8<&f%version%&8> &7[&3%certainty%&7]")
            .build();

}
